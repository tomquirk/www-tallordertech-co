---
title: "5 reasons why Slack will reject your Slack app"
date: 2020-10-09T10:25:06+09:30
draft: false
image: "cover.jpg"
categories:
  - Blog
tags:
  - Slack app
  - Engineering
---

At a time when businesses are increasingly relying on Slack for day-to-day operations, the Slack App Directory is thriving. These days, there are Slack apps for everything: from [polls](https://slack.com/apps/A04E6JX41-polly) to [team recognition](https://slack.com/apps/A0J4UNFLN-heytaco), to [daily standups](https://slack.com/apps/A355V71K7-standupiy), to [promoting inclusive language](https://slack.com/apps/A01A18SMWCC-allybot). There has never been a better time to be a Slack user.

Likewise, there has never been a better time to create a Slack app!

But it's not all sunshine and roses. I've been through the Slack app process twice, first with [UPPIT](https://slack.com/apps/ARPAT12GY-uppit) and now with [AllyBot](https://slack.com/apps/A01A18SMWCC-allybot). Building a Slack app is fun and Slack is always improving the [developer experience](<(https://twitter.com/slackapi/status/1121107098895392768)>). But, there are a few gotchas that have cost me a lot of time during the review process.

Here are 5 reasons why Slack will reject your bot, and what you can do about it.

## 1. Security

Do yourself a favor and use [bolt.js](https://github.com/boltpkg/bolt). This one might be obvious, but it certainly wasn't to me when I built [UPPIT](https://uppit.io/). Don't get me wrong, you can build a basic Slack app with the Slack web API and your backend framework of choice; this is what I did with UPPIT.

But when you go to submit your app to the Slack App Directory, you will soon realize there are a bunch of security things you probably didn't consider. OAuth and token management, and [verifying requests from Slack](https://api.slack.com/authentication/verifying-requests-from-slack) and the ones I've failed with in the past. Both these topics deserve an article, but the takeaway is that bolt.js handles a lot of this stuff for you. It provides sensible interfaces and callbacks for the things you need to customize. It also provides simple wrappers for the Slack web API out of the box. bolt.js an obvious choice that allows you to focus on building your app.

I estimate 20% of the time I spent on UPPIT was on these auxiliary tasks, whereas when I used bolt.js for [AllyBot](https://allybot.io/), it was more like 5%.

Don't reinvent the wheel - use bolt.js!

## 2. No customer support or poor branding

When you're in the trenches building your app, it's easy to forget that you need to market the thing! Slack requires a bunch of items on this front. Let's go through some of the ways you can succeed on this front.

### Privacy policy and Terms of Service

Slack will not accept your app without a link to a Privacy Policy and a Terms of Service. It's best to seek legal advice, but you can also get pretty far by using existing Slack apps as inspiration.

### Support email address and contact form

You need an email address for support and a support page on your website. For email, I use [Zoho](https://www.zoho.com/). It's free, and I've set up tom@allybot.io, and a "catch-all" alias that points to tom@allybot.io. This means I can list my app's support contact as something like support@allybot.io and I'll receive any emails sent there at tom@allybot.io.

For the support page, a simple contact form does the job - check out [https://allybot.io/#support](https://allybot.io/#support) for an example (built with [https://carrd.co](carrd.co)).

### Add to Slack button

Make sure your website has an Add to Slack button. If you're using bolt.js, link this button to https://<your-app.com>/slack/install. Redirect the user to an "Install Successful" page once they've successfully installed your app (Slack requires this).

### Branding

Slack has a bunch of [resources](https://slack.dev/guides/AppUIGuidelines.pdf) on this topic, but the bottom line is this: keep your branding consistent and don't conflict with Slack's branding. Simple!

## 3. Bad use of the App Home

Utilizing the App Home in Slack can be confusing, but this is a must-do. Slack has a [whole article](https://api.slack.com/surfaces/tabs/events) on this, but here are some essentials.

### Send the user who installed your app an intro message

When a user installs your app, you need to send them a message that explains to them how to get started. Here is [AllyBot](https://allybot.io/)'s welcome message:

![AllyBot's installed welcome message](allybot-installed-welcome-message.png)

Of course, make sure you only send this once.

### Respond to app_home_opened event

When a user other than the user that installed your app opens your apps App Home tab, you need to greet them as well! Again, this should only be a once-off. I have a flag for app_home_opened in the user table of the database, set to false. When the user first opens the App Home tab, this flag flips to true so I know never to send the message to the user again. The message can be like the first one, for example, this is how [AllyBot.io](https://allybot.io/) responds to app_home_opened:

![AllyBot's user welcome message](allybot-user-welcome-message.png)

Note that you will need to request the `im:write` scope to start chats with users.

## 4. Not giving good reasons for your requested OAuth scopes

This is a common reason Slack apps get held up in the review process. You should take the time to consider if your app needs all the scopes you are requesting. Slack will reject anything that seems like a "nice to have".

Give genuine reasons about why you need a certain scope. And be transparent. If you are capturing user emails (with the `users:read.email` scope), then say so.

The same applies to your data retention policies. Don't simply say "We will retain your data as per our privacy policy". Give a short, clear and concise description about how you plan to handle your user's data. Something like "When you ask us to delete your data, we will respond to you via email by 24 hours. We will aim to fulfill your data removal request with 24 hours of responding, and will inform you when your data has been removed. For more information, see our Privacy Policy".

## 5. Poor error handling

During the review process, Slack will try and break your app. But don't see this as a negative; we should be super grateful to get free QA testing (thanks Slack)! Using bolt.js will help you here. But, if your Slack app uses slash commands, or "actions" (buttons, etc.), think carefully about where your app could fail.

Make sure to send the user a message when errors occur. An ["ephemeral"](https://api.slack.com/methods/chat.postEphemeral) message is a good way to do this; an inline, private message to the user interacting with your app. Something as simple as "Oops, something went wrong 😢" is a good start, but always try to [give direction to the user](https://uxplanet.org/how-to-write-good-error-messages-858e4551cd4).

## Ask me anything

Have I missed something? Need something clarified? Hit me up on Twitter [@tom\_\_quirk](https://twitter.com/tom__quirk).
